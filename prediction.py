'''
simple program that does predictions
of body and brain weights
'''
import pandas as pd
from sklearn import linear_model
import matplotlib.pyplot as plt

datafram = pd.read_fwf('brain_body.txt')
x_values = dataframe[['brain']]
y_values = dataframe[['body']]

body_reg = linear_model.LinearRegression()
body_reg.fit(x_values, y_values)

plt.scatter(x_values, y_values)
plt.plot(x_values, body_reg.predict(x_values))
plt.show()
